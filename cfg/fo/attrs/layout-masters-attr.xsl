<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs"
   version="2.0">
   <xsl:variable name="fullArtPrefix"
      select="concat($artworkPrefix, 'Customization/OpenTopic/common/artwork/')"/>

   <!-- Cover page -->
   <xsl:attribute-set name="region-body__frontmatter.odd"
      use-attribute-sets="region-body.odd">
      <xsl:attribute name="margin-bottom">0pt</xsl:attribute>
      <xsl:attribute name="margin-top">0cm</xsl:attribute>
      <xsl:attribute name="margin-left">0pt</xsl:attribute>
      <xsl:attribute name="margin-right">0pt</xsl:attribute>
      <xsl:attribute name="background-image">
         <xsl:value-of select="concat('url(', $fullArtPrefix, 'cover.png)')"/>
      </xsl:attribute>
      <xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
   </xsl:attribute-set>

   <xsl:attribute-set name="region-after-even">
      <xsl:attribute name="extent">
         <xsl:value-of select="$page-margin-bottom"/>
      </xsl:attribute>
      <xsl:attribute name="display-align">after</xsl:attribute>
      <xsl:attribute name="background-image">
         <xsl:value-of
            select="concat('url(', $fullArtPrefix, 'footer-left.png)')"/>
      </xsl:attribute>
      <xsl:attribute name="background-repeat">no-repeat</xsl:attribute>
   </xsl:attribute-set>

   <xsl:attribute-set name="region-after-odd">
      <xsl:attribute name="extent">
         <xsl:value-of select="$page-margin-bottom"/>
      </xsl:attribute>
      <xsl:attribute name="display-align">after</xsl:attribute>
      <xsl:attribute name="background-image">
         <xsl:value-of
            select="concat('url(', $fullArtPrefix, 'footer-right.png)')"/>
      </xsl:attribute>
      <xsl:attribute name="repeat">no-repeat</xsl:attribute>
   </xsl:attribute-set>

</xsl:stylesheet>
