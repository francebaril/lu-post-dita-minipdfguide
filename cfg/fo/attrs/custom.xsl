<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="2.0">
  <xsl:import href="commons-attr.xsl"/>
  <xsl:import href="front-matter-attr.xsl"/>
  <xsl:import href="layout-masters-attr.xsl"></xsl:import>
  <!-- Change page size to custom format -->
  <xsl:variable name="page-width">150mm</xsl:variable>
  <xsl:variable name="page-height">150mm</xsl:variable>
</xsl:stylesheet>