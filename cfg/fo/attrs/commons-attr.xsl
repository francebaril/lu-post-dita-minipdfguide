<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:xs="http://www.w3.org/2001/XMLSchema"
   exclude-result-prefixes="xs"
   version="2.0">
    <!-- *** Set defaults *** -->
    <xsl:attribute-set name="__fo__root" use-attribute-sets="base-font">
        <!-- Change color and font -->
        <xsl:attribute name="color">#333333</xsl:attribute>
        <xsl:attribute name="font-family">Tahoma</xsl:attribute>
        <!-- Justify most text -->
        <xsl:attribute name="text-align">left</xsl:attribute>
        <xsl:attribute name="line-height">
            <xsl:value-of select="$default-line-height"/>
        </xsl:attribute>
        <!--xsl:attribute name="font-family">serif</xsl:attribute-->
        <!-- TODO: https://issues.apache.org/jira/browse/FOP-2409 -->
        <xsl:attribute name="xml:lang" select="translate($locale, '_', '-')"/>
        <xsl:attribute name="writing-mode" select="$writing-mode"/>
    </xsl:attribute-set>
    
    
    <!-- *** Add to modify heading 1 in output *** -->
    <xsl:attribute-set name="topic.title"
        use-attribute-sets="common.title">
        <xsl:attribute name="border-after-width">0pt</xsl:attribute>
        <xsl:attribute name="space-before">0pt</xsl:attribute>
        <xsl:attribute name="space-after">0pt</xsl:attribute>
        <xsl:attribute name="color">#00b2dd</xsl:attribute>
        <xsl:attribute name="font-size">18pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="padding-bottom">16.8pt</xsl:attribute>
        <xsl:attribute name="padding-top">16.8pt</xsl:attribute>
        <xsl:attribute name="keep-with-next.within-column"
            >always</xsl:attribute>
    </xsl:attribute-set>
</xsl:stylesheet>