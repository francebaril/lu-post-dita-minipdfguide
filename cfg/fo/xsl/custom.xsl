<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:xs="http://www.w3.org/2001/XMLSchema"
   xmlns:fo="http://www.w3.org/1999/XSL/Format" version="2.0">
   <!-- Move figure title to top and description to bottom -->
   <xsl:import href="layout-masters.xsl"/>
   <xsl:import href="static-content.xsl"/>
   
   <xsl:variable name="mirror-page-margins" select="true()"/>
   
   
   <xsl:template match="*[contains(@class, ' topic/fig ')][contains(@outputclass, 'video-tutorial')]"><!-- Nothing, we want to skip the videos in pdf --></xsl:template>
</xsl:stylesheet>
